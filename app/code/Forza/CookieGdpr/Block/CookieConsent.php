<?php

namespace Forza\CookieGdpr\Block;

use Magento\Framework\View\Element\Template;

class CookieConsent extends Template {

  CONST COOKIE_LIFETIME_DAYS = 31;

  protected $helperData;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Forza\CookieGdpr\Helper\Data $helperData
	)
	{
		$this->helperData = $helperData;
		return parent::__construct($context);
	}

  public function isModuleEnabled() {

    if($this->helperData->getGeneralConfig('enable') == 1) {
      return true;
    } else {
      return false;
    }

  }

  public function getStyleAttribute($attributeName) {

    return $this->helperData->getStylesConfig($attributeName);

  }

  public function getGeneralCookieText() {

    return $this->helperData->getGeneralConfig('display_text');

  }

  public function getGeneralPrivacyLinkText() {

    return $this->helperData->getGeneralConfig('privacy_link_text');

  }

  public function getCookiePolicyLink() {

    return $this->helperData->getGeneralConfig('privacy_link');

  }

}
