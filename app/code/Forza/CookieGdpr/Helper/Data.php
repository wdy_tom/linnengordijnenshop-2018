<?php
namespace Forza\CookieGdpr\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

Class Data extends AbstractHelper {

  const XML_PATH_COOKIES = 'cookies/';

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_COOKIES .'general/'. $code, $storeId);
	}

  public function getStylesConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_COOKIES .'styles/'. $code, $storeId);
	}

  public function getFunctionalData($code, $storeId = null)
  {
    return $this->getConfigValue(self::XML_PATH_COOKIES .'functional/'. $code, $storeId);
  }

  public function getPerformanceData($code, $storeId = null)
  {
    return $this->getConfigValue(self::XML_PATH_COOKIES .'performance/'. $code, $storeId);
  }

  public function getSocialData($code, $storeId = null)
  {
    return $this->getConfigValue(self::XML_PATH_COOKIES .'social/'. $code, $storeId);
  }

}
