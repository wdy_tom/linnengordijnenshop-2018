<?php

namespace Forza\HeroImage\Block;

use Magento\Framework\View\Element\Template;

class Image extends Template {

  protected $resourceConnection;
  protected $page;

 	public function __construct(
 		\Magento\Framework\View\Element\Template\Context $context,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Magento\Cms\Model\Page $page
 	)
 	{
 	  parent::__construct($context);
    $this->resourceConnection = $resourceConnection;
    $this->page = $page;
 	}

  public function getCurrentPageId() {
    return $this->page->getId();
  }

  public function fetchImage() {

    $connection = $this->resourceConnection->getConnection();
    $tableName  = $this->resourceConnection->getTableName('cms_page');
    $sql = "SELECT * FROM " . $tableName ." WHERE page_id='".$this->getCurrentPageId()."'";
    $result = $connection->fetchAll($sql);
    return $result;

  }

 }
