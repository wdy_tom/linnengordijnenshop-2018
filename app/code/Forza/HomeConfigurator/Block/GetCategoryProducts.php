<?php

namespace Forza\HomeConfigurator\Block;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

Class Category extends \Magento\Framework\App\Action\Action {

  protected $_productCollectionFactory;
  protected $_categoryFactory;
  protected $resultsJsonFactory;

  public function __construct(

    \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
    \Magento\Catalog\Model\CategoryFactory $categoryFactory,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    \Magento\Framework\App\Action\Context $context
  )
  {
    $this->resultPageFactory = $resultPageFactory;
    $this->jsonData = $jsonData;
    $this->_categoryFactory = $categoryFactory;
    $this->_productCollectionFactory = $productCollectionFactory;
    $this->resultJsonFactory = $resultJsonFactory;
    parent::__construct($context);
  }

  public function getCategoryId() {
    $categoryId = $this->getRequest()->getParam('selectedCategory');
    return $categoryId;
  }

  public function getProductCollection() {
    $categoryId = $this->getCategoryId();
    $category = $this->_categoryFactory->create()->load($categoryId);
    $collection = $this->_productCollectionFactory->create();
    $collection->addAttributeToSelect('*');
    $collection->addCategoryFilter($category);
    $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
    $collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);

    return $collection;
  }

  public function execute() {
    $productCollection = $this->getProductCollection();
    $result = $this->resultJsonFactory->create();
    return $result->setData($productCollection);
  }

}
