<?php

namespace Forza\HomeConfigurator\Block;

use Magento\Framework\view\Element\Template;

Class Steps extends Template {

  protected $_categoryCollection;
  protected $_productCollection;
  protected $_categoryFactory;
  protected $_productOption;

  public function __construct(
       \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
       \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
       \Magento\Catalog\Model\CategoryFactory  $categoryFactory,
       \Magento\Catalog\Model\Product\Option $productOption,
       \Magento\Framework\View\Element\Template\Context $context
  ) {
        $this->_categoryCollection = $categoryCollection;
        $this->_productCollection = $productCollection;
        $this->_categoryFactory = $categoryFactory;
        $this->_productOption = $productOption;
        parent::__construct($context);
  }

  public function getCategoryCollection()
  {
    $categoryIds = '4,5,6,14,15,16,25';

     $collection = $this->_categoryCollection
                        ->create()
                        ->addAttributeToSelect('image')
                        ->addFieldToFilter('entity_id', array('in' => $categoryIds));

     return $collection;
   }

   public function getCategoryModel($id)
   {
       $_category = $this->_categoryFactory->create();
       $_category->load($id);
       return $_category;
   }

   public function getProductOptions($id)
   {
     $productOptions = $this->_productOption->getProductOptionCollection($id);
     return $productOptions;
   }

   public function getCategoryProducts($categoryId) {

    $category = $this->_categoryCollection->create()->load($categoryId);
    $collection = $this->_productCollection->create();
    $collection->addAttributeToSelect('*')
               ->addCategoryFilter($category)
               ->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
               ->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);

    return $collection;

   }



}
