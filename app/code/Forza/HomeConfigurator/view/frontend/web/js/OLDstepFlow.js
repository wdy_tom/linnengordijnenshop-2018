require([
    "jquery",
], function($){

  // Selected Class for Product
  const AddSelector = function() {
    $(".options__image--container-product").on('click', function(){
      $(this).find('.options__image--overlay').addClass('options__image--selected').siblings().removeClass('options__image--selected');
      $(this).parent().addClass('selected-product').siblings().removeClass('selected-product');
    });
  }

  //Progress bar
  $(".next").on('click', function() {

    const containerParent = $(this).parents().eq(3);
    const progressBar = $('.progress__bar li.active');

    $(containerParent).hide();
    // $(containerParent).next().css('display', 'block');
    $(containerParent).next().show();
    $(progressBar).next().addClass('active');

    $('.step--number').text(parseInt($('.step--number').text()) + 1);
  });

  //selected class for category
  $(".options__image--container-category").on('click', function(){
    $(this).find('.options__image--overlay').addClass('options__image--selected').siblings().removeClass('options__image--selected');
    $(this).parent().addClass('selected-category').siblings().removeClass('selected-category');
  });


  //Changing image on click
  $(".options__image--overlay").on('click', function() {
    // $('.config-product__image--large').attr('src', $('.options__image').attr("src"));
    var productImage = $('.options__image').attr("src");
    $('.config-product__image--large').attr('src', productImage);
  });



  //category ajax call
  $(".category-next").on('click', function() {

    var selectedCategoryId = $('.selected-category').attr('data-target');
    console.log(selectedCategoryId);
    $.ajax({
      showLoader : true,
      type : 'POST',
      url : '/getcategory/category/getproducts',
      // timeout : 12000,
      data : {
        'selectedCategory' : selectedCategoryId,
      },
      dataType : 'json',
      encode : true,

      success : function(data) {
         $('.products__collection').html('byeeeee');
          // console.log(data);
          // _.each(data, function(item, index) {
          //   $('.products__collection').append('<div class="width-33 match-height"><div class="individual--option" data-target="'+item.entity_id+'"><div class="options__image--container options__image--container-product"><img src="/media/catalog/product'+item.thumbnail+'" class="options__image"/><div class="options__image--overlay">  <i class="fa fa-check-circle options__image--check"></i></div></div><p class="options__title">'+item.name+'</p></div></div>');
          //   AddSelector();

        // }, this);
      },
    });

  });

  // Product Details AJAX call
  $(".product-next").on('click', function() {
    var selectedProductId = $('.selected-product').attr('data-target');
    console.log(selectedProductId);
    $.ajax({
      showLoader : true,
      type : 'POST',
      url : '/getproducts/products/productdetails',
      // timeout : 12000,
      data : {
        'selectedProduct' : selectedProductId,
      },
      dataType : 'json',
      encode : true,
      success : function(data) {
        $('.options__product--details').html(data['description']);
        console.log(data);
      },
    });
  });



});
