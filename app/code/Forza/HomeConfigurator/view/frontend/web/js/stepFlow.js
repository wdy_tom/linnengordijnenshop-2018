require([
    "jquery",
], function($){

  // Selected Class for Product
  const AddSelector = function() {
    $(".options__image--container-product").on('click', function(){
      $(this).find('.options__image--overlay').addClass('options__image--selected').siblings().removeClass('options__image--selected');
      $(this).parent().addClass('selected-product').siblings().removeClass('selected-product');
    });
  }

  //Progress bar
  $(".next").on('click', function() {

    const containerParent = $(this).parents().eq(3);
    const progressBar = $('.progress__bar li.active');

    $(containerParent).hide();
    // $(containerParent).next().css('display', 'block');
    $(containerParent).next().show();
    $(progressBar).next().addClass('active');

    // $('.step--number').text(parseInt($('.step--number').text()) + 1);
  });

  //selected class for category
  $(".options__image--container-category").on('click', function(){
    $(this).find('.options__image--overlay').addClass('options__image--selected').siblings().removeClass('options__image--selected');
    $(this).parent().addClass('selected-category').siblings().removeClass('selected-category');
  });


  //Changing image on click
  $(".options__image--overlay").on('click', function() {
    // $('.config-product__image--large').attr('src', $('.options__image').attr("src"));
    var productImage = $('.options__image').attr("src");
    $('.config-product__image--large').attr('src', productImage);
  });




});
