<?php

namespace Forza\SeperatingCartProducts\Model\Quote;

class Item extends \Magento\Quote\Model\Quote\Item {

  public function representProduct($product)
  {

    $itemProduct = $this->getProduct();
    if (!$product || $itemProduct->getId() != $product->getId()) {
        return false;
    }

    $stickWithinParent = $product->getStickWithinParent();
    if ($stickWithinParent) {
        if ($this->getParentItem() !== $stickWithinParent) {
            return false;
        }
    }

    $itemOptions = $this->getOptionsByCode();
    $productOptions = $product->getCustomOptions();

    if (!$this->compareOptions($itemOptions, $productOptions)) {
        return false;
    }
    if (!$this->compareOptions($productOptions, $itemOptions)) {
        return false;
    }
    return false;
  }

}
