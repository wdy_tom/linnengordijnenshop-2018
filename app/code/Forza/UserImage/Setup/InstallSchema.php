<?php


namespace Forza\UserImage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
      $installer = $setup;

      $installer->startSetup();

      $eavTable = $installer->getTable('admin_user');

      $columns = [
          'user_image' => [
              'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              'nullable' => false,
              'comment' => 'User Image',
          ],

      ];

      $connection = $installer->getConnection();
      foreach ($columns as $name => $definition) {
          $connection->addColumn($eavTable, $name, $definition);
      }

      $installer->endSetup();
    }
}
