<?php

namespace Forza\configurator\Block\Product\View;

class Curtain extends \Magento\Framework\View\Element\Template {

	public function isConfigProduct() {

		// Getting the current product
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_product = $objectManager->get('Magento\Framework\Registry')
    													->registry('current_product');
    	return ($_product->getData('tonen_configurator') == '1');

	}

	public function isRomanBlind() {

		// Getting the current product
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_product = $objectManager->get('Magento\Framework\Registry')
															->registry('current_product');
			return ($_product->getData('roman_configurator') == '1');

	}

	protected $_registry;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }


}
