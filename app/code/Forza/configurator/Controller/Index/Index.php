<?php

namespace Forza\configurator\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Forza\configurator\Model\PriceCalculation;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $priceCalculation;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Forza\configurator\Model\PriceCalculation $PriceCalculation

    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->priceCalculation = $PriceCalculation;

    }

    public function execute()
    {

        $prodId = $this->getRequest()->getParam('product_id');
        $width = $this->getRequest()->getParam('width');
        $height = $this->getRequest()->getParam('height');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($prodId);
        $file = $product->getData('select_csv_data');
        $price = $this->priceCalculation->init($file)->getPrice($width, $height);
        $result = $this->resultJsonFactory->create();
        return $result->setData(["price" => $price]);

    }
    

}
