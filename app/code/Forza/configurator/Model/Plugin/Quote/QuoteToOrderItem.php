<?php
namespace Forza\configurator\Model\Plugin\Quote;

use Closure;

class QuoteToOrderItem
{
    /**
     * @param \Magento\Quote\Model\Quote\Item\ToOrderItem $subject
     * @param callable $proceed
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param array $additional
     * @return \Magento\Sales\Model\Order\Item
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {

        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setHeight($item->getHeight());
        $orderItem->setWidth($item->getWidth());
        $orderItem->setLeftWidth($item->getLeftWidth());
        $orderItem->setRightWidth($item->getRightWidth());


        $orderOptions = $orderItem->getProductOptions();
        $orderOptions['options'][] = array(
            'label' => 'breedte',
            'value' => $item->getWidth().'cm',
            'print_value' =>  $item->getWidth().'cm' ,
            'option_id' => '9000',
            'custom_view' => false,
        );
        $orderOptions['options'][] = array(
            'label' => 'Hoogte',
            'value' => $item->getHeight().'cm',
            'print_value' =>  $item->getHeight().'cm' ,
            'option_id' => '9001',
            'custom_view' => false,
        );
        if($item->getLeftWidth()) {
            $orderOptions['options'][] = array(
                'label' => 'Links',
                'value' => $item->getLeftWidth() . 'cm',
                'print_value' => $item->getLeftWidth() . 'cm',
                'option_id' => '9002',
                'custom_view' => false,
            );
        }
        if($item->getRightWidth()) {
            $orderOptions['options'][] = array(
                'label' => 'Rechts',
                'value' => $item->getRightWidth() . 'cm',
                'print_value' => $item->getRightWidth() . 'cm',
                'option_id' => '9003',
                'custom_view' => false,
            );
        }
        $orderItem->setProductOptions($orderOptions);


        return $orderItem;

    }

}