<?php

namespace Forza\configurator\Model;

class PriceCalculation {

	public $fileName;
	public $pricingData;

	public function init($file){

		$this->fileName = $file;

		$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$currentStore = $storeManager->getStore();
		$baseUrl = $currentStore->getBaseUrl();

		$csvData = file_get_contents($baseUrl . 'CSV/'.$this->fileName.'.csv', 'r');
		$csvData = explode(PHP_EOL, $csvData);

		foreach ($csvData as $csvRow) {
			$rowData[] = str_getcsv($csvRow, ';');
		}

		$widthIndex = $rowData[0];
		unset($rowData[0]);
		foreach ($rowData as $row) {
			$height = $row[0];
			unset($row[0]);
			foreach ($row as $index => $value) {
				$pricingRow[$height][$widthIndex[$index]]=$value;
			}
		}

		$this->pricingData = $pricingRow;
		return $this;

	}

	public function getPrice($width, $height) {
		$pricingData = $this->pricingData;
		$widthPricing = $this->_findUpperValueOfArray($pricingData, $height);
		$finalPrice = $this->_findUpperValueOfArray($widthPricing, $width);
		return $finalPrice;

	}


	public function _findUpperValueOfArray($array,$sizeValue)
	{
		foreach ($array as $key => $value) {
			if($key-$sizeValue > -1){
				return $value;
			}
		}
		return false;
	}


}
