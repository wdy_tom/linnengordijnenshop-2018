<?php

namespace Forza\configurator\Observer;

use Magento\Framework\Event\ObserverInterface;

class QuoteToSale implements ObserverInterface
{

 	public function execute(\Magento\Framework\Event\Observer $observer)
 	{
		$quote = $observer->getEvent()->getQuote();
		$order = $observer->getEvent()->getOrder();

		$sizes = [];
    if (empty($quote->getData('width'))) {
      return;
    }
		foreach ($quote->getItems() as $key => $_item) {
			$sizes[$key]['width'] = $_item->getData('width');
			$sizes[$key]['height'] = $_item->getData('height');
			$sizes[$key]['left_width'] = $_item->getData('left_width');
			$sizes[$key]['right_width'] = $_item->getData('right_width');

		}

		foreach ($order->getItems() as $key => $_item) {
			// var_dump(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));die;
			// var_dump($_item->setData('width',$sizes[$key]['width']));
			if ( !isset($sizes[$key])) {
				continue;
			}
			$_item->setData('width',$sizes[$key]['width']);
			$_item->setData('height',$sizes[$key]['height']);
			$_item->setData('left_width',$sizes[$key]['left_width']);
			$_item->setData('right_width',$sizes[$key]['right_width']);
			$_item->save();

		}

 	}
 }
