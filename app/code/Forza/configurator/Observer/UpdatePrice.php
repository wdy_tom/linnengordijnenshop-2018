<?php

namespace Forza\configurator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Forza\configurator\Model\PriceCalculation;


    class UpdatePrice implements ObserverInterface
    {
    	protected $_request;
    	protected $PriceCalculation;

    	public function __construct(
		    \Magento\Framework\App\RequestInterface $request,
		    PriceCalculation $PriceCalculation
		) {
			$this->priceCalculation = $PriceCalculation;
		    $this->_request = $request;
		}

    public function execute(\Magento\Framework\Event\Observer $observer) {
      $prodId = $this->_request->getParam('product');
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $product = $objectManager->create('Magento\Catalog\Model\Product')->load($prodId);
      $file = $product->getData('select_csv_data');
      if($file == ''){
        return;
      }

      $width = $this->_request->getParam('width');
      $height = $this->_request->getParam('height');
      $leftWidth = $this->_request->getParam('left_width');
      $rightWidth = $this->_request->getParam('right_width');

      $price = $this->priceCalculation->init($file)->getPrice($width, $height);

      $item = $observer->getEvent()->getData('quote_item');
      $item = ($item->getParentItem() ? $item->getParentItem() : $item );

      $item->setData('height', $height);
      $item->setData('width', $width);
      $item->setData('left_width', $leftWidth);
      $item->setData('right_width', $rightWidth);

      $item->setCustomPrice($price);
      $item->setOriginalCustomPrice($price);
      $item->getProduct()->setIsSuperMode(true);

    }
    }
