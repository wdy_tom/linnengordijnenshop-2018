<?php

namespace Forza\configurator\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
 
 
class InstallSchema implements InstallSchemaInterface
{   
 
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        
        $connection = $setup->getConnection();
 
            $column1 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('quote_item'), 'height', $column1);
            $column2 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('quote_item'), 'width', $column2);
            $column3 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('quote_item'), 'left_width', $column3);
            $column4 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('quote_item'), 'right_width', $column4);
            $column5 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('sales_order_item'), 'height', $column5);
            $column6 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('sales_order_item'), 'width', $column6);
            $column7 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('sales_order_item'), 'left_width', $column7);
            $column8 = [
                'type' => Table::TYPE_TEXT,
                'length' => 32,
                'nullable' => false,
                'comment' => 'Height',
                'default' => '0'
            ];
            $connection->addColumn($setup->getTable('sales_order_item'), 'right_width', $column8);


    }
}