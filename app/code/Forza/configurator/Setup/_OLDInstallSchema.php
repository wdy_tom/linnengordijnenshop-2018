<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Forza\configurator\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $installer = $setup;

        $installer->startSetup();

        $quoteTable = $installer->getTable('quote_item');
        $columns = [
            'height' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'left_width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'right_width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ]
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($quoteTable, $name, $definition);
        }

        $salesTable = $installer->getTable('sales_order_item');
        $columns = [
            'height' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'left_width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'right_width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ],
            'width' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '32',
                'nullable' => false,
                'comment' => 'Masked ID',
            ]
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($salesTable, $name, $definition);
        }

        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'select_csv_data');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'select_csv_data',/* Custom Attribute Code */
            [
                'group' => 'Product Details',
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => 'Select CSV Pricing',
                'input' => 'select',
                'class' => '',
                'source' => 'Forza\configurator\Model\Config\Source\Options',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );
        }

        $installer->endSetup();






     //    $installer = $setup;
     //    $installer->startSetup();

     //   $table = $installer->getConnection(
     //    $installer->getTable('sales_order_item')
     //    )->addColumn(
     //    'height',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
    	// )
     //    ->addColumn(
     //    'width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    )
     //    ->addColumn(
     //    'left_width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    )
     //    ->addColumn(
     //    'right_width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    );
     //    $installer->endSetup();

     //    $table = $installer->getConnection(
    	// $installer->getTable('sales_order_item')
    	// )->addColumn(
     //    'height',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    )
     //    ->addColumn(
     //    'width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    )
     //    ->addColumn(
     //    'left_width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    )
     //    ->addColumn(
     //    'right_width',
     //    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
     //    32,
     //    ['nullable' => 'false'],
     //    'Masked ID'
     //    );

     //    $installer->endSetup();

    }
}