define([
    "jquery",
], function($){
    $.fn.productConfigurator = function(method) {
      if(methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
      } else if(typeof method === 'object' || ! method){
        return methods.init.apply(this, arguments);
      }	else {
        $.error('Method ' +  method + ' does not exist.');
      }
    };

    var settings, $el;
    var values = {
      width : 0,
      height : 0,
      left : 0,
      right : 0,
      productID : undefined
    };

    var methods = {
      // Init method
      init : function(options) {
        // Set all default and override specific settings
        settings = $.extend({
          productFieldSelector : 'input[name="product"]',
          widthFieldSelector : '.width-field',
          heightFieldSelector : '.height-field',
          leftFieldSelector : '#left_width',
          rightFieldSelector : '#right_width',
          priceLabelOldSelector : '.price',
          priceLabelNewSelector : '.new-price',
          pricePrefix : '&euro; ',
          priceSuffix : '',
          priceURL : 'calculate/index/index',
          decimalSeparator : ',',
          thousandSeparator : '.',
          precision : 2,
          successHandler : null,
          errorHandler : null,
          errorClass : 'mage-error'
        }, options);

        $el = $(this);

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));
            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };

        return this.each(function() {
          $(this).productConfigurator('bindEvents');
          values.productID = $(settings.productFieldSelector).val();
          $el.productConfigurator('enableFields');
          console.log('>>> Product configurator initialized.');
        });
      },

      bindEvents: function() {
        return this.each(function() {
          $el.find(settings.widthFieldSelector).on('change.product-configurator', function(e) {
            values.width = Number(parseFloat($(this).val()));
            $(this).productConfigurator('calculate');
          });
          $el.find(settings.heightFieldSelector).on('change.product-configurator', function(e) {
            values.height = Number(parseFloat($(this).val()));
            $(this).productConfigurator('calculate');
          });
          $el.find(settings.leftFieldSelector).on('change.product-configurator', function(e) {
            values.left = Number(parseFloat($(this).val()));
            $(this).productConfigurator('validateSizes');
          });
          $el.find(settings.rightFieldSelector).on('change.product-configurator', function(e) {
            values.right = Number(parseFloat($(this).val()));
            $(this).productConfigurator('validateSizes');
          });
          $el.find('.toggle').each(function(e) {
            var self = this;
            $(this).find('.info-img').on('click', function(e) {
              $(self).toggleClass("expanded");
              $(self).next().slideToggle();
            });
          });
          // disable mousewheel on a input number field when in focus
          $('#product_addtocart_form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
              e.preventDefault()
            })
          });
          $('#product_addtocart_form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
          });
        });
      },

      // Unhook all set events
      unbindEvents : function( ) {
        return this.each(function() {
          // Unbind all namespaced events
          $(window).unbind('.product-configurator');
        });
      },

      disableFields: function() {
        return this.each(function() {
          $el.find(settings.widthFieldSelector)
              .add(settings.heightFieldSelector)
              .add(settings.leftFieldSelector)
              .add(settings.rightFieldSelector).prop('disabled', true);
        });
      },

      enableFields: function() {
        return this.each(function() {
          $el.find(settings.widthFieldSelector)
              .add(settings.heightFieldSelector)
              .add(settings.leftFieldSelector)
              .add(settings.rightFieldSelector).prop('disabled', false);
        });
      },

      // Format and set the price
      setPrice: function(price) {
        return this.each(function() {
          $(settings.priceLabelOldSelector).addClass('hide-price');
          $(settings.priceLabelOldSelector).hide();
          $(settings.priceLabelNewSelector).html(settings.pricePrefix + price.format(settings.precision, 0, settings.thousandSeparator, settings.decimalSeparator) + settings.priceSuffix);
          $(".product-add-form,.radio,.admin__control-radio,.product-custom-option").click(function() {
            $(".price").hide();
          });
          $('.price-label').hide();
        });
      },

      validateSizes: function() {
        return this.each(function() {
          if (values.left == 0 || values.right == 0) {
            $el.find(settings.leftFieldSelector).add(settings.rightFieldSelector).removeClass(settings.errorClass);
          } else if ((values.left + values.right) != values.width) {
            $el.find(settings.leftFieldSelector).add(settings.rightFieldSelector).addClass(settings.errorClass);
          } else {
            $el.find(settings.leftFieldSelector).add(settings.rightFieldSelector).removeClass(settings.errorClass);
          }
        });
      },

      calculate: function() {
        return this.each(function() {
          $el.productConfigurator('shouldFetchPrice', function(shouldFetchPrice) {
            if (shouldFetchPrice) {
              $el.productConfigurator('fetchPrice', {
                success: function(message) {
                  if (settings.successHandler) {
                    settings.successHandler(message);
                  }
                },
                error: function(message) {
                  if (settings.errorHandler) {
                    settings.errorHandler(message);
                  }
                }
              });
            }
          });
        });
      },

      shouldFetchPrice : function(callback) {
        return this.each(function() {
          if (callback) {
            callback((values.width && values.height));
          }
        });
      },

      fetchPrice : function(options) {
        return this.each(function() {
          $el.productConfigurator('disableFields');
          $.ajax({
            showLoader : true,
            type : 'POST',
            url : settings.priceURL,
            timeout : 12000,
            data : {
              'width' : values.width,
              'height' : values.height,
              'product_id' : values.productID
            },
            dataType : 'json',
            encode : true,
            success : function(data) {
              $el.productConfigurator('enableFields');
              $el.productConfigurator('setPrice', Number(data.price));
              if (options.success) {
                 if (data.price == '') {
                  $('.new-price').html('Prijs op aanvraag')
                } else {
                  options.success('Price Updated.');
                }
                // if (!Number.isNaN(data.price)) {
                //   options.success('Price updated.');
                // } else {
                //   options.error('Sorry, er zijn geen prijzen beschikbaar voor deze maat');
                // }
              }
            },
            error: function(xmlhttprequest, textstatus, message) {
              $el.productConfigurator('enableFields');
              if (options.error) {
                options.error(message);
              }
            }
          });
        });
      }
    };
  });
