<?php

namespace Forza\navigationlinks\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Data\Tree\NodeFactory;

class TopmenuObserver implements ObserverInterface
{

    protected $nodeFactory;

    public function __construct(
        NodeFactory $nodeFactory
    ) {
        $this->nodeFactory = $nodeFactory;
    }


    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $menu = $observer->getMenu();

        $tree = $menu->getTree();

        foreach ($this->getNodesAsArray() as $index => $nodeAsArray) {
            $node = $this->nodeFactory->create([
                'data' => $nodeAsArray,
                'idField' => 'id',
                'tree' => $tree
            ]);

            $menu->addChild($node);

        }
    }


    protected function getNodesAsArray()
    {
        return [[
            'name' => __('Rail & Roede'),
            'class' => 'hide-mobile',
            'id' => 'ophangmethode',
            'url' => '/ophangmethode',
            'has_active' => false,
            'is_active' => false
        ],[
            'name' => __('Inspiratie'),
            'class' => 'hide-mobile',
            'id' => 'inspiratie',
            'url' => '/inspiratie',
            'has_active' => false,
            'is_active' => false
        ], [
            'name' => __('Contact'),
            'class' => 'hide-mobile',
            'id' => 'contact',
            'url' => '/contact',
            'has_active' => false,
            'is_active' => false
        ]];

    }
}
