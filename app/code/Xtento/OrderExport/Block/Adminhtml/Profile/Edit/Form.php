<?php

/**
 * Product:       Xtento_OrderExport (2.3.6)
 * ID:            Amt4Gsn/+mY9PM33BCVmGNibW69eKOU987rYSYS/Ow4=
 * Packaged:      2017-09-12T11:48:59+00:00
 * Last Modified: 2015-08-05T18:56:20+00:00
 * File:          app/code/Xtento/OrderExport/Block/Adminhtml/Profile/Edit/Form.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Block\Adminhtml\Profile\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}