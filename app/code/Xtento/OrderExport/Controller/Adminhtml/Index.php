<?php

/**
 * Product:       Xtento_OrderExport (2.3.6)
 * ID:            Amt4Gsn/+mY9PM33BCVmGNibW69eKOU987rYSYS/Ow4=
 * Packaged:      2017-09-12T11:48:59+00:00
 * Last Modified: 2016-04-28T15:48:36+00:00
 * File:          app/code/Xtento/OrderExport/Controller/Adminhtml/Index.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Controller\Adminhtml;

abstract class Index extends \Xtento\OrderExport\Controller\Adminhtml\Action
{
    /**
     * Check if user has enough privileges
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @param $resultPage \Magento\Backend\Model\View\Result\Page
     */
    protected function updateMenu($resultPage)
    {
        $resultPage->setActiveMenu('Xtento_OrderExport::profiles');
        $resultPage->addBreadcrumb(__('Sales'), __('Sales'));
        $resultPage->addBreadcrumb(__('Sales Export'), __('Sales Export'));
        $resultPage->getConfig()->getTitle()->prepend(__('Sales Export'));
    }
}
