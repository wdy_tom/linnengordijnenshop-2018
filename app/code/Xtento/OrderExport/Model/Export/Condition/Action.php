<?php

/**
 * Product:       Xtento_OrderExport (2.3.6)
 * ID:            Amt4Gsn/+mY9PM33BCVmGNibW69eKOU987rYSYS/Ow4=
 * Packaged:      2017-09-12T11:49:00+00:00
 * Last Modified: 2016-02-25T18:46:04+00:00
 * File:          app/code/Xtento/OrderExport/Model/Export/Condition/Action.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Model\Export\Condition;

class Action extends \Magento\Rule\Model\Condition\Combine
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Action constructor.
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param array $data
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        array $data = []
    ) {
        $this->_eventManager = $eventManager;
        parent::__construct($context, $data);
        $this->setType('Xtento\OrderExport\Model\Export\Condition\Action');
    }

    /**
     * Get new child select options
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        return [];
    }
}
