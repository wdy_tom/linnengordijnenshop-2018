<?php

/**
 * Product:       Xtento_OrderExport (2.3.6)
 * ID:            Amt4Gsn/+mY9PM33BCVmGNibW69eKOU987rYSYS/Ow4=
 * Packaged:      2017-09-12T11:49:00+00:00
 * Last Modified: 2017-03-06T13:10:17+00:00
 * File:          app/code/Xtento/OrderExport/Model/Export/Config/SchemaLocator.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Model\Export\Config;

use Magento\Framework\Module\Dir;

class SchemaLocator implements \Magento\Framework\Config\SchemaLocatorInterface
{
    /**
     * Path to corresponding XSD file with validation rules for merged config
     *
     * @var string
     */
    protected $_schema;

    /**
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     */
    public function __construct(\Magento\Framework\Module\Dir\Reader $moduleReader)
    {
        $this->_schema = $moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Xtento_OrderExport') . '/' . 'xtento/orderexport_data.xsd';
    }

    /**
     * {@inheritdoc}
     */
    public function getSchema()
    {
        return $this->_schema;
    }

    /**
     * {@inheritdoc}
     */
    public function getPerFileSchema()
    {
        return null;
    }
}
