<?php

/**
 * Product:       Xtento_OrderExport (2.3.6)
 * ID:            Amt4Gsn/+mY9PM33BCVmGNibW69eKOU987rYSYS/Ow4=
 * Packaged:      2017-09-12T11:48:59+00:00
 * Last Modified: 2016-04-17T13:03:38+00:00
 * File:          app/code/Xtento/OrderExport/Observer/SalesOrderInvoicePayObserver.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\OrderExport\Observer;

use Xtento\OrderExport\Model\Export;

class SalesOrderInvoicePayObserver extends AbstractEventObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->handleEvent($observer, self::EVENT_SALES_ORDER_INVOICE_PAY, Export::ENTITY_INVOICE);
        $this->handleEvent($observer, self::EVENT_SALES_ORDER_INVOICE_PAY, Export::ENTITY_ORDER);
    }
}
