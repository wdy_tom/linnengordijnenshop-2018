const gulp   = require('gulp');
const sass   = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const watch  = require('gulp-watch');

const basePath = './app/design/frontend/Forza/linnengordijnenshop';

gulp.task('watch', function () {
  gulp.watch(basePath + '/styles/**/*.scss', ['mincss']);
});

gulp.task('sass', function () {
  return gulp.src(basePath + '/styles/styles.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(basePath + '/web/css/'));
});

gulp.task('mincss', ['sass'], function () {
  gulp.src(basePath + '/web/css/styles.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(basePath + '/web/css/'));
    gulp.start('copystatic');
});

gulp.task('copystatic', function() {
    gulp.src(basePath+'/web/css/styles.css')
    // Perform minification tasks, etc here
    .pipe(gulp.dest('./pub/static/frontend/Forza/linnengordijnenshop/en_GB/css'));
});

gulp.task('requireJsMinify', function() {
    gulp.src('./pub/static/_requirejs/frontend/Forza/linnengordijnenshop/**/*.js')
        .pipe(minify({
            ext: {
                min: '.js'
            },
            noSource: {}
        }))
        // .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./pub/static/_requirejs/frontend/Forza/linnengordijnenshop/'));
});

gulp.task('jsMinify', function() {
    gulp.src('./pub/static/frontend/Forza/linnengordijnenshop/**/*.js')
        .pipe(minify({
            ext: {
                min: '.js'
            },
            mangle: false,
            noSource: {}
        }))
        .pipe(debug())
        // .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./pub/static/frontend/Forza/linnengordijnenshop/'));
});

gulp.task('default', ['mincss', 'watch'], function () {
});

gulp.task('java', ['requireJsMinify', 'jsMinify'], function () {
});
